import {combineReducers} from '@reduxjs/toolkit'
import productSlice from './productSlice/productSlice'

const rootState = combineReducers({
  productSlice: productSlice
})

export default rootState